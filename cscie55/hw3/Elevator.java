/** 
 * Elevator is a simulation of a building elevator system with a simple sweep algorithm,
 * which ensures that no passenger can get stranded no matter how busy the building.
 * 
 * @author Bret Fontecchio
 * @version 3.0
 * @since November 9, 2013
 */

package cscie55.hw3;

import java.util.*;

public class Elevator {

	// Constants
	public static final int CAPACITY;
	public static final int NUM_FLOORS;

	public static enum Direction {
		UP, DOWN, NOWHERE
	};

	// Instance Variables
	public Direction dir;
	protected int floor;
	protected int numPassengers;

	/**
	 * objects of type Floor which represent each floor and can hold waiting
	 * passengers.
	 */
	private Floor[] floors;

	/**
	 * floors requested by boarded passengers or requested for pickup by
	 * potential passengers on the floor. Requested floors are stored separately
	 * in Sets corresponding to direction. They are swapped into and out of
	 * the floorsRequested Set, which is visible to the program code.
	 */
	private Set<Integer> goingUp, goingDown;
	protected Set<Integer> floorsRequested;

	/**
	 * Passengers destined to each floor in the array index.
	 * should have <code>NUM_FLOORS</code> number of elements.
	 */
	protected ArrayList<Queue<Passenger>> passengersDestined;

	static {
		CAPACITY = 10;
		NUM_FLOORS = 7;
	}

	/**
	 * Elevator default constructor - zero-arg constructor which can be
	 * overloaded and perhaps chained. <code>passengersDestined</code>
	 * initialization should fill array with zero passengers per floor.
	 */
	public Elevator() {
		this.floor = 1;
		this.dir = Direction.UP;
		this.passengersDestined = new ArrayList<Queue<Passenger>>(NUM_FLOORS+1);
		for (int i = 0; i <= NUM_FLOORS; i++) {
			passengersDestined.add(new LinkedList<Passenger>());
		}
		this.goingUp = new HashSet<Integer>();
		this.goingDown = new HashSet<Integer>();
		this.floorsRequested = goingUp;
		this.floors = new Floor[NUM_FLOORS + 1];
		for (int i = 0; i <= NUM_FLOORS; i++) {
			floors[i] = new Floor(i);
		}
	}

	/**
	 * toString overrides Object.toString, gives a text description of Elevator
	 * state. System.out.println(myElevator); will print state. As of version
	 * 3.0, toString prints the state of all passengers onboard.
	 * 
	 * @return state the state of the Elevator object.
	 */
	public String toString() {
		String state = String
				.format("%n%s%d%s%n%s%d%n", "Currently ", numPassengers,
						" passengers on board", "Current Floor: ", floor);
		for (Queue<Passenger> q : passengersDestined) {
			for (Passenger p : q) {
				state += String.format("%s%d%n", "Passenger destined to floor: ",
					p.getDestFloor());
			}
		}
		return state;
	}

	/**
	 * registerRequest adds the destination floor to the list of floors
	 * requested. There are two lists of requested floors, separated by
	 * direction of requested travel: <code>goingUp</code> and <code>
	 * goingDown</code>
	 * 
	 * @param destFloor		the floor requested
	 * @param dir			the direction requested
	 */
	public void registerRequest(int destFloor, Direction dir) {
		Set<Integer> set;
		if (dir == Direction.UP) {
			set = goingUp;
		} else {
			set = goingDown;
		}
		set.add(destFloor);
	}

	/**
	 * Increments or decremenents the <code>floor</code>, changing
	 * <code>dir</code> at edge cases.
	 * <p>
	 * As of version 2.0, <code>move</code> does not check for a valid
	 * direction. That check is done by the compiler, as <code>Direction</code>
	 * is an enum. In the else clause, it is assumed that <code>dir</code> must
	 * be <code>Direction.DOWN</code>.
	 * </p>
	 */
	private void move() {
		if (dir == Direction.UP) {
			if (floor < NUM_FLOORS) {
				floor++;
			} else {
				dir = Direction.DOWN;
				floorsRequested = goingDown;
				move();
			}
		} else {
			if (floor > 1) {
				floor--;
			} else {
				dir = Direction.UP;
				floorsRequested = goingUp;
				move();
			}
		}
	}

	/**
	 * Simulates passengers de-boarding the Elevator at a destination.
	 * <code>numPassengers
	 * </code>, the number of passengers onboard, is decremented as appropriate.
	 * The number of passengers destined for that floor is cleared. A check is
	 * done to make sure we don't put <code>numPassengers</code> into the
	 * negatives. This is handled silently, and may cover a bug!
	 * <p>
	 * stop unloads passengers onto the floor, then loads passengers from the
	 * floor by passing control to Floor.
	 * </p>
	 * <p>
	 * The <code>floor</code> has to be boxed explicitly as an
	 * <code>Integer</code> because <code>ArrayList</code> has a
	 * <code>remove(int)</code> method, but it is not what we're looking for.
	 * Auto-boxing will not occur here.
	 * </p>
	 * <p>
	 * Although the floor is removed from floorsRequested, it may be added back
	 * by Floor if the elevator is full and the Floor wishes to load more
	 * passengers to it. A request can be re-registered during the behavior that
	 * takes place in <code>unloadPassengers</code>.
	 */
	private void stop() {
		String msg = String.format("%n%s%d", "Stopping on floor ", floor);
		System.out.println(msg);
		floors[floor].unloadPassengers(this);
		System.out.println(this);
	}

	/**
	 * Simulates a single passenger boarding, destined for a given floor.
	 * 
	 * @param destFloor
	 *            the floor this passenger is destined to.
	 */
	public void boardPassenger(Passenger p) throws ElevatorFullException {
		if (!(numPassengers < CAPACITY)) {
			String msg = String.format("%n%s%d",
					"boarding passenger on floor: ", floor);
			throw new ElevatorFullException(msg);
		}
		int destFloor = p.getDestFloor();
		registerRequest(destFloor, p.getDir());
		passengersDestined.get(destFloor).add(p);
		numPassengers++;
	}

	/**
	 * Our simulation, which is a particular instance of <code>Elevator</code>,
	 * traveling to our <code>Floors</code> and boarding passengers as we see
	 * fit for demonstration purposes.
	 * <p>
	 * We use the print method to implicitly call toString, and then begin the
	 * simulation loop. The <code>stop</code> and <code>boardPassenger</code>
	 * methods will update stdout from there.
	 * </p><p>
	 * The basic routine is to create a Passenger, who has a currentFloor and a
	 * destFloor, and place that passenger onto that floor. Alternatively, they
	 * can be boarded into the elevator and given a currentFloor 1 since we are
	 * starting the simulation on floor 1 (see Elevator constructor).
	 * </p>
	 */
	public static void main(String[] args) {
		int sweeps = 6;
		Elevator elevator = new Elevator();
		// put 5 passengers on floor 3, headed to floor 4
		for (int n = 0; n < 5; n++) {
			elevator.floors[3].addPassenger(new Passenger(3, 4));
		}
		// put 50 passengers on floor 5, headed to floor 1
		for (int n = 0; n < 50; n++) {
			elevator.floors[5].addPassenger(new Passenger(5, 1));
		}
		// put a passenger on floor 1, headed to the top
		try {
			elevator.boardPassenger(new Passenger(1, Elevator.NUM_FLOORS));
		} catch (ElevatorFullException e) {
			System.out
				.println("Attempted to board too many passengers. Elevator is full.");
		}
		System.out.print(elevator);
		for (int i = 1; i <= sweeps * Elevator.NUM_FLOORS * 2; i++) {
			if (elevator.floorsRequested.contains(elevator.floor)
					|| elevator.floors[elevator.floor].isCalling(elevator)) {
				elevator.stop();
			}
			elevator.move();
		}
	}
}
