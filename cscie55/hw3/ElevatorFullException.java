/** ElevatorFullException throws information up the call stack pertaining to an over
 *  capacity elevator board attempt. Optionally stores overflow amount.
 *  <p>
 *  Calls super constructors.
 *  </p>
 */

package cscie55.hw3;

public class ElevatorFullException extends Exception {

	/**
	 * Automatically generated by Eclipse.
	 */
	private static final long serialVersionUID = -5216774023965159204L;

	ElevatorFullException() {
		super();
	}

	ElevatorFullException(String msg) {
		super("ElevatorFullException: " + msg);
	}

}