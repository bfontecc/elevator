/**
 * Class Passenger represents a person who may or may not interact with the <code>Elevator</code>.
 */

package cscie55.hw3;

import cscie55.hw3.Elevator.Direction;

class Passenger {
	private int currentFloor;
	private int destFloor;

	public Passenger(int currentFloor, int destFloor) {
		this.currentFloor = currentFloor;
		this.destFloor = destFloor;
	}
	
	public void arrive() {
		String msg = String.format("%s%d%s%d", "Passenger arrived on floor ",
				currentFloor, ". Destination was: ", destFloor);
		System.out.println(msg);
	}
	
	public int getCurrentFloor() {
		return currentFloor;
	}

	public void setCurrentFloor(int currentFloor) {
		this.currentFloor = currentFloor;
	}

	public int getDestFloor() {
		return destFloor;
	}

	public void setDestFloor(int destFloor) {
		this.destFloor = destFloor;
	}
	
	public Direction getDir() {
		if (destFloor > currentFloor) {
			return Direction.UP;
		} else if (destFloor == currentFloor) {
			return Direction.NOWHERE;
		} else {
			return Direction.DOWN;
		}
	}

}