/**
 * Class Floor represents a floor in a building, which may have passengers waiting for an
 * <code>Elevator</code>. Stores the <code>Passenger</code> objects in three data structures:
 * 	residents <code>ArrayList</code> stores passengers who remain on that floor
 * 	goingUp and goingDown <code>Queues</code> store passengers with destinations. 
 */

package cscie55.hw3;

import java.util.*;

import cscie55.hw3.Elevator.Direction;

class Floor {
	private int floorNumber;
	private Queue<Passenger> residents, goingUp, goingDown;
	
	/**
	 * Floor constructor with a floor number.
	 */
	protected Floor(int floorNumber) {
		this.floorNumber = floorNumber;
		this.residents = new LinkedList<Passenger>();
		this.goingUp = new LinkedList<Passenger>();
		this.goingDown = new LinkedList<Passenger>();
	}
	
	/**
	 * Add passenger to the floor. Passenger may be a resident, or may be going
	 * up or down.
	 */
	public void addPassenger(Passenger p) {
		if (p.getDir() == Direction.UP) {
			goingUp.add(p);
		} else if (p.getDir() == Direction.DOWN) {
			goingDown.add(p);
		} else {
			residents.add(p);
		}
	}
	
	public boolean isCalling(Elevator e) {
		if (e.dir == Direction.UP && goingUp.size() > 0) {
			return true;
		} else if (e.dir == Direction.DOWN && goingDown.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Unload passengers from the Elevator onto the floor. Board the passengers from 
	 * the floor onto the <code>Elevator</code> if possible. Calls methods in 
	 * <code>Elevator</code> and handles any Exceptions that occur.
	 * 
	 * @param elevator
	 *            Elevator object which has arrived at this Floor.
	 */
	public void unloadPassengers(Elevator e) {
		int gotOff = 0;
		Queue<Passenger> gettingOff = e.passengersDestined.get(e.floor);
		while(gettingOff.peek() != null) {
			Passenger p = gettingOff.remove();
			p.setCurrentFloor(e.floor);
			p.arrive();
			residents.add(p);
			gotOff++;
		}
		e.numPassengers -= gotOff;
		// e.floor is an item in the Collection, not an index, hence Object passed
		e.floorsRequested.remove(new Integer(e.floor));
		Queue<Passenger> gettingOn;
		if (e.dir == Direction.UP) {
			gettingOn = goingUp;
		} else {
			gettingOn = goingDown;
		}
		while (gettingOn.peek() != null) {
			Passenger p = gettingOn.remove();
			try {
				e.boardPassenger(p);
			} catch (ElevatorFullException ex) {
				e.registerRequest(floorNumber, p.getDir());
				gettingOn.add(p);
				break;
			}
		}
	}
}